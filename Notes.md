# General Interactive Firewall Notes

## Programming Architecture (pseudocode for batch file)
- Print help menu when necessary
- Print disclaimers
- Confirm with user windows firewall is in use
- Backup existing firewall rules and profiles
- Reset all rules and profiles to a clean slate
- Get TCP/TCPv6/UDP/UDPv6 listeners
- Apply TCP/TCPv6/UDP/UDPv6 listeners
- Enable deny-all's after an item in the menu has been selected
- Start menu containing:
	a. Remove listener
	b. List rules
	c. Show counters
	d. exit

If a listener is chosen:
- Cycle through the listener array and remove listener
- Delete all implemented rules
- Re-apply all listeners without deleted listener

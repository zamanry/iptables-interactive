#!/bin/bash

# Issues:
# Medium vs Hard vs Extreme
#

function reset_tables () { # resets IPv4/6 chains/tables and adds basic rules
	local debug stateful_module stateful_option mode block_inbound_icmp
	debug="$1"
	stateful_module="$2"
	stateful_option="$3"
	mode="$4"
	block_inbound_icmp="$5"


	# resets IPv4 rules/chains
	iptables -P INPUT ACCEPT # Temporarily placed to not break any traffic states
	iptables -P OUTPUT ACCEPT # Temporarily placed to not break any traffic states
	iptables -F # Flush all rules
	iptables -X # Delete all chains
	#iptables -Z # Zero all counters
	iptables -t nat -F # Flush all NAT rules
	iptables -t nat -X # Delete all NAT chains
	#iptables -t nat -Z # Zero all NAT counters
	iptables -t mangle -F # Flush all mangle rules
	iptables -t mangle -X # Delete all mangle chains
	#iptables -t mangle -Z # Zero all mangle counters
	iptables -P FORWARD DROP # Drop all IP forwarding/routing since this is an end device


	# resets IPv6 rules/chains
	ip6tables -P INPUT ACCEPT # Temporarily placed to not break any traffic states
	ip6tables -P OUTPUT ACCEPT # Temporarily placed to not break any traffic states
	ip6tables -F # Flush all rules
	ip6tables -X # Delete all chains
	#ip6tables -Z # Zero all counters
	ip6tables -t nat -F # Flush all NAT rules
	ip6tables -t nat -X # Delete all NAT chains
	#ip6tables -t nat -Z # Zero all NAT counters
	ip6tables -t mangle -F # Flush all mangle rules
	ip6tables -t mangle -X # Delete all mangle chains
	#ip6tables -t mangle -Z # Zero all mangle counters
	ip6tables -P FORWARD DROP # Drop all IP forwarding/routing since this is an end device


	if [[ "${mode}" == 'hard' ]] || [[ "${mode}" == 'extreme' ]]; then # if Hard or Extreme, implement common network attack rules
		iptables -A INPUT -p tcp ! --syn -m "$stateful_module" "$stateful_option" NEW -j DROP # Drop any TCP packet which don't begin with a syn flag
		iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP # Drop any TCP packet which has every flag set
		iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP # Drop any TCP glag packet which has all flags set, but a null header
		iptables -A INPUT -f -j DROP # Drop any fragmented packet


		ip6tables -A INPUT -p tcp ! --syn -m "$stateful_module" "$stateful_option" NEW -j DROP # Drop any TCP packet which don't begin with a syn flag
		ip6tables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP # Drop any TCP packet which has every flag set
		ip6tables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP # Drop any TCP glag packet which has no flag set
	fi


	if [[ ! "${block_inbound_icmp}" ]]; then # if block all inbound ICMP not requested, enable it
		# Read about ICMP states here https://www.frozentux.net/iptables-tutorial/chunkyhtml/x1582.html
		iptables -A INPUT -p icmp --icmp-type echo-request -m "$stateful_module" "$stateful_option" NEW -j ACCEPT # Accept echo-requests and Windows traceroute
		iptables -A INPUT -p udp --dport 33434:33523 -j REJECT # Reject UNIX traceroute (by rejecting, tracert succeeds, but doesn't open the port)
	fi


	# IPv4 stateful rules and restrict localhost
	iptables -A INPUT -m "$stateful_module" "$stateful_option" RELATED,ESTABLISHED -j ACCEPT
	iptables -A OUTPUT -m "$stateful_module" "$stateful_option" RELATED,ESTABLISHED -j ACCEPT
	iptables -A OUTPUT -s 127.0.0.0/8 -d 127.0.0.0/8 -o lo -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT


	# temporaryily added to not break outbound traffic
	iptables -A OUTPUT -p tcp --dport 443 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # HTTPS
	iptables -A OUTPUT -p tcp --dport 80 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # HTTP
	iptables -A OUTPUT -p udp --dport 53 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # DNS
	iptables -A OUTPUT -p udp --dport 123 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # NTP
	iptables -A OUTPUT -p udp --sport 68 --dport 67 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # DHCP
	iptables -A OUTPUT -p tcp --dport 22 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # DHCP
	iptables -A OUTPUT -p tcp --dport 1194 -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # DHCP
	iptables -A OUTPUT -p icmp --icmp-type echo-request -m "$stateful_module" "$stateful_option" NEW,ESTABLISHED -j ACCEPT #ICMP echo-request


	# IPv6 stateful rules and restrict localhost
	ip6tables -A INPUT -m "$stateful_module" "$stateful_option" RELATED,ESTABLISHED -j ACCEPT
	ip6tables -A OUTPUT -m "$stateful_module" "$stateful_option" RELATED,ESTABLISHED -j ACCEPT
	ip6tables -A OUTPUT -s ::1/128 -d ::1/128 -o lo -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT


	if [[ "${debug}" ]]; then # if debug mode requested, enable logging
		iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPv4 INPUT drops: '
		iptables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPv4 OUTPUT drops: '
		ip6tables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPv6 INPUT drops: '
		ip6tables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPv6 OUTPUT drops: '
	fi


	unset debug stateful_module stateful_option mode block_inbound_icmp
}


function backup_tables () {	# backups the tables before resetting them
	local debug ipv4_backup ipv6_backup timestamp
	debug="$1"
	ipv4_backup="$2"
	ipv6_backup="$3"
	timestamp=$(date +%m-%d-%Y-%H-%M) # Month-Day-Year-Hour-Minute


	[[ ! "${ipv4_backup}" ]] && iptables-save > "$timestamp".iptables # if an IPv4 backup is not requested, don't
	[[ ! "${ipv4_backup}" ]] && [[ "${debug}" ]] && echo "[Debug] Backed-up IPv4 rules to $timestamp.iptables"
	[[ ! "${ipv6_backup}" ]] && ip6tables-save > "$timestamp".ip6tables # if an IPv6 backup is not requested, don't
	[[ ! "${ipv6_backup}" ]] && [[ "${debug}" ]] && echo "[Debug] Backed-up IPv6 rules to $timestamp.ip6tables"


	unset debug ipv4_backup ipv6_backup timestamp
}


function detect_firewall_packages () { # determine if iptables, ufw, and firewalld are installed
	local debug response
	debug="$1"

	if ! dpkg -s iptables >/dev/null 2>&1; then
		echo "[i] Install iptables before advancing."
		exit 1
	else
		[[ "${debug}" ]] && echo "[Debug] iptables package found"
	fi


	if dpkg -s ufw >/dev/null 2>&1; then
		read -r -p '[i] UFW was found installed. You must remove UFW before advancing; would you like to remove it now? (Y/n): ' response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')"
		if [ "$response" == 'y' ]; then
			echo "[i] Removing UFW..."
			apt-get purge ufw -y > /dev/null
			echo "[i] Removed UFW."
		else
			echo "[i] Remove UFW before advancing."
			exit 1
		fi
	else
		[[ "${debug}" ]] && echo "[Debug] UFW package not found"
	fi


	if dpkg -s firewalld >/dev/null 2>&1; then
		read -r -p '[i] firewalld was found installed. You must remove firewalld before advancing; would you like to remove it now? (Y/n): ' response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')"
		if [[ $(echo "$response" | grep -o '') != 'n' ]]; then
			echo "[i] Removing firewalld..."
			apt-get purge firewalld -y > /dev/null
			echo "[i] Removed firewalld."
		else
			echo "[i] Remove firewalld before advancing."
			exit 1
		fi
	else
		[[ "${debug}" ]] && echo "[Debug] firewalld package not found"
	fi


	unset debug response
}


function cycle_listeners() {	# cycles the listener array after a listener is to be deleted
	local debug listener_to_delete cycled_listeners
	debug="$1"
	listener_to_delete="$2"


	[[ "${debug}" ]] && echo "[Debug] Removing $listener_to_delete in $proto listeners"

	if [ "$proto" = "tcp6" ]; then # if a tcp listener was chosen
		for i in "${tcp6_listeners[@]}"; do
			[[ "$i" != "$listener_to_delete" ]] && cycled_listeners+=("$i")
			[[ "$i" != "$listener_to_delete" ]] && [[ "$debug" ]] && echo "[Debug] Adding $i to cycled_listeners"
		done
		tcp6_listeners=("${cycled_listeners[@]}") # copies the cycled listeners
	elif [ "$proto" = "udp6" ]; then
		for i in "${udp6_listeners[@]}"; do
			[[ "$i" != "$listener_to_delete" ]] && cycled_listeners+=("$i")
			[[ "$i" != "$listener_to_delete" ]] && [[ "$debug" ]] && echo "[Debug] Adding $i to cycled_listeners"
		done
		udp6_listeners=("${cycled_listeners[@]}") # copies the cycled listeners
	elif [ "$proto" = "tcp" ]; then # if a tcp listener was chosen
		for i in "${tcp4_listeners[@]}"; do
			[[ "$i" != "$listener_to_delete" ]] && cycled_listeners+=("$i")
			[[ "$i" != "$listener_to_delete" ]] && [[ "$debug" ]] && echo "[Debug] Adding $i to cycled_listeners"
		done
		tcp4_listeners=("${cycled_listeners[@]}") # copies the cycled listeners
	elif [ "$proto" = "udp" ]; then
		for i in "${udp4_listeners[@]}"; do
			[[ "$i" != "$listener_to_delete" ]] && cycled_listeners+=("$i")
			[[ "$i" != "$listener_to_delete" ]] && [[ "$debug" ]] && echo "[Debug] Adding $i to cycled_listeners"
		done
		udp4_listeners=("${cycled_listeners[@]}") # copies the cycled listeners
	fi
	unset debug listener_to_delete cycled_listeners
}


function cycle_states () {	# cycles the states array after a state is to be deleted when creating a new rule
	local debug state_to_delete cycled_states
	debug="$1"
	state_to_delete="$2"

	[[ "${debug}" ]] && echo "[Debug] Removing $state_to_delete"

	for i in "${states[@]}"; do
		[[ "$i" != "$state_to_delete" ]] && cycled_states+=("$i")
		[[ "$i" != "$state_to_delete" ]] && [[ "$debug" ]] && echo "[Debug] Adding $i to cycled_states"
	done
	states=("${cycled_states[@]}") # copies the cycled states

	unset debug state_to_delete cycled_states
}


function get_listeners () { # gets listener ports and processes and combines them
	local tcp4_ports tcp4_processes udp4_ports udp4_processes tcp6_ports tcp6_processes udp6_ports udp6_processes i de_duplicated_listeners

	if [ "$(command -v ss)" != "" ]; then
		[[ "${debug}" ]] && echo "[Debug] Using the ss command"

		# tcp processes in netstat and ss are broken. For whatever reason, the grep command is not finding the 2nd ". Using cut worked though. Really odd.
		# Worked on Mint and Lubuntu

		mapfile -t tcp4_ports < <(ss -tlpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\:)\d*(?=\s)')
		# mapfile -t tcp4_processes < <(ss -tlpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\").*(?=\")')
		mapfile -t tcp4_processes < <(ss -tlpn4 | grep -v 'Recv\-Q\|\s127\.' | cut -d \" -f 2)
		mapfile -t udp4_ports < <(ss -ulpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t udp4_processes < <(ss -ulpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\").*(?=\")')

		mapfile -t tcp6_ports < <(ss -tlpn6 | grep -v 'Recv\-Q\|\:\:1' | grep -Po '(?<=\:)\d*(?=\s)')
		# mapfile -t tcp6_processes < <(ss -tlpn6 | grep -v 'Recv\-Q\|\:\:1' | grep -Po '(?<=\").*(?=\")')
		mapfile -t tcp6_processes < <(ss -tlpn6 | grep -v 'Recv\-Q\|\:\:1' | cut -d \" -f 2)
		mapfile -t udp6_ports < <(ss -ulpn6 | grep -v 'Recv\-Q\|\:\:1' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t udp6_processes < <(ss -ulpn6 | grep -v 'Recv\-Q\|\:\:1' | grep -Po '(?<=\").*(?=\")')
	elif [ "$(command -v netstat)" != "" ]; then
		[[ "${debug}" ]] && echo "[Debug] Using the netstat command"

		mapfile -t tcp4_ports < <(netstat -tlpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\:)\d*(?=\s)')
		# mapfile -t tcp4_processes < <(netstat -tlpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\/).*\S')
		mapfile -t tcp4_processes < <(netstat -tlpn4 | grep -v 'Recv\-Q\|\s127\.' | cut -d \" -f 2)
		mapfile -t udp4_ports < <(netstat -ulpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t udp4_processes < <(netstat -ulpn4 | grep -v 'Recv\-Q\|\s127\.' | grep -Po '(?<=\/).*\S')

		mapfile -t tcp6_ports < <(netstat -tlpn6 | grep -v 'Recv\-Q\|\s\:\:1' | grep -Po '(?<=\:)\d*(?=\s)')
		# mapfile -t tcp6_processes < <(netstat -tlpn6 | grep -v 'Recv\-Q\|\s\:\:1' | grep -Po '(?<=\/).*\S')
		mapfile -t tcp6_processes < <(netstat -tlpn6 | grep -v 'Recv\-Q\|\s\:\:1' | cut -d \" -f 2)
		mapfile -t udp6_ports < <(netstat -ulpn6 | grep -v 'Recv\-Q\|\s\:\:1' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t udp6_processes < <(netstat -ulpn6 | grep -v 'Recv\-Q\|\s\:\:1' | grep -Po '(?<=\/).*\S')
	elif [ "$(command -v lsof)" != "" ]; then
		[[ "${debug}" ]] && echo "[Debug] Using the lsof command"

		# Neither processes are not being caught
		mapfile -t tcp4_ports < <(lsof -iTCP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv6' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t tcp4_processes < <(lsof -iTCP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv6' | grep -Po '(?<=\").*(?=\")')
		mapfile -t udp4_ports < <(lsof -iUDP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv6' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t udp4_processes < <(lsof -iUDP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv6' | grep -Po '(?<=\").*(?=\")')

		mapfile -t tcp6_ports < <(lsof -iTCP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv4' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t tcp6_processes < <(lsof -iTCP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv4' | grep -Po '(?<=\").*(?=\")')
		mapfile -t udp6_ports < <(lsof -iUDP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv4' | grep -Po '(?<=\:)\d*(?=\s)')
		mapfile -t udp6_processes < <(lsof -iUDP -P -n | grep -v 'Recv\-Q\|\s127\.\|IPv4' | grep -Po '(?<=\").*(?=\")')
	else
		echo "Please install either ss, netstat, or lsof commands before advancing."
		exit 1
	fi


	# Create IPv4 listener arrays (proto4:port:process)
	i=0
	for port in "${tcp4_ports[@]}"; do
		tcp4_listeners+=(tcp:"$port":"${tcp4_processes["$i"]}") # combine everything to look like "tcp:port:process"
		i=$((i+1)) # iterate
	done
	i=0
	for port in "${udp4_ports[@]}"; do
		udp4_listeners+=(udp:"$port":"${udp4_processes["$i"]}") # combine everything to look like "udp:port:process"
		i=$((i+1)) # iterate
	done


	# Create IPv6 listener array (proto6:port:process)
	i=0
	for port in "${tcp6_ports[@]}"; do
		tcp6_listeners+=(tcp6:"$port":"${tcp6_processes["$i"]}") # combine everything to look like "tcp:port:process"
		i=$((i+1)) # iterate
	done
	i=0
	for port in "${udp6_ports[@]}"; do
		udp6_listeners+=(udp6:"$port":"${udp6_processes["$i"]}") # combine everything to look like "udp:port:process"
		i=$((i+1)) # iterate
	done


	if [[ "${debug}" ]]; then # if debug mode requested, enable logging
		echo "[Debug] Logging enabled. Example usage: 'grep -r 'IPv4 INPUT' /var/log/*'"
		echo "[Debug] Raw tcp4_port(s) = ${tcp4_ports[*]}"
		echo "[Debug] Raw tcp4_process(es) = ${tcp4_processes[*]}"
		echo "[Debug] Raw tcp4_listener(s) = ${tcp4_listeners[*]}"
		echo "[Debug] Raw udp4_port(s) = ${udp4_ports[*]}"
		echo "[Debug] Raw udp4_process(es) = ${udp4_processes[*]}"
		echo "[Debug] Raw udp4_listener(s) = ${udp4_listeners[*]}"


		echo "[Debug] Raw tcp6_port(s) = ${tcp6_ports[*]}"
		echo "[Debug] Raw tcp6_process(es) = ${tcp6_processes[*]}"
		echo "[Debug] Raw tcp6_listener(s) = ${tcp6_listeners[*]}"
		echo "[Debug] Raw udp6_port(s) = ${udp6_ports[*]}"
		echo "[Debug] Raw udp6_process(es) = ${udp6_processes[*]}"
		echo "[Debug] Raw udp6_listener(s) = ${udp6_listeners[*]}"
	fi


	# de-duplicates any TCP listeners (ie. a listener for NTP for 0.0.0.0/0 and 192.168.4.6)
	for listener in "${tcp4_listeners[@]}"; do # for TCP
		if [[ ! "${de_duplicated_listeners[*]}" =~ $listener ]]; then
			de_duplicated_listeners+=("$listener")
		fi
	done
	tcp4_listeners=("${de_duplicated_listeners[@]}") # adds the de-duplicated listeners back to the global listener array


	declare -a de_duplicated_listeners=()
	for listener in "${udp4_listeners[@]}"; do # for UDP
		if [[ ! "${de_duplicated_listeners[*]}" =~ $listener ]]; then
			de_duplicated_listeners+=("$listener")
		fi
	done
	udp4_listeners=("${de_duplicated_listeners[@]}") # adds the de-duplicated listeners back to the global listener array


	declare -a de_duplicated_listeners=()
	for listener in "${tcp6_listeners[@]}"; do # for TCP
		if [[ ! "${de_duplicated_listeners[*]}" =~ $listener ]]; then
			de_duplicated_listeners+=("$listener")
		fi
	done
	tcp6_listeners=("${de_duplicated_listeners[@]}") # adds the de-duplicated listeners back to the global listener array


	declare -a de_duplicated_listeners=()
	for listener in "${udp6_listeners[@]}"; do # for UDP
		if [[ ! "${de_duplicated_listeners[*]}" =~ $listener ]]; then
			de_duplicated_listeners+=("$listener")
		fi
	done
	udp6_listeners=("${de_duplicated_listeners[@]}") # adds the de-duplicated listeners back to the global listener array


	if [[ "${debug}" ]]; then # if debug, enable logging
		echo "[Debug] De-duplicated input tcp4_listener(s) = ${tcp4_listeners[*]}"
		echo "[Debug] De-duplicated input udp4_listener(s) = ${udp4_listeners[*]}"
		echo "[Debug] De-duplicated input tcp6_listener(s) = ${tcp6_listeners[*]}"
		echo "[Debug] De-duplicated input udp6_listener(s) = ${udp6_listeners[*]}"
	fi


	unset de_duplicated_listeners tcp4_ports tcp4_processes udp4_ports udp4_processes tcp6_ports tcp6_processes udp6_ports udp6_processes
}


function apply_listeners () {	# implement de-duplicated listeners
	local listener_port stateful_module stateful_option
	stateful_module="$1"
	stateful_option="$2"


	for listener in "${tcp4_listeners[@]}"; do # apply TCP listeners
		listener_port=$(echo "$listener" | grep -Po '(?<=:)(.*)(?=:)') # get the listener's port from (tcp:port:process)
		iptables -A INPUT -p tcp --dport "$listener_port" -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # apply TCP ports
	done
	for listener in "${udp4_listeners[@]}"; do # apply UDP listeners
		listener_port=$(echo "$listener" | grep -Po '(?<=:)(.*)(?=:)') # get the listener's port from (udp:port:process)
		iptables -A INPUT -p udp --dport "$listener_port" -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # apply UDP ports
	done


	for listener in "${tcp6_listeners[@]}"; do # apply TCP listeners
		listener_port=$(echo "$listener" | grep -Po '(?<=:)(.*)(?=:)') # get the listener's port from (tcp6:port:process)
		ip6tables -A INPUT -p tcp --dport "$listener_port" -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # apply TCP ports
	done
	for listener in "${udp6_listeners[@]}"; do # apply UDP listeners
		listener_port=$(echo "$listener" | grep -Po '(?<=:)(.*)(?=:)') # get the listener's port from (udp6:port:process)
		ip6tables -A INPUT -p udp --dport "$listener_port" -m "$stateful_module" "$stateful_option" NEW,RELATED,ESTABLISHED -j ACCEPT # apply UDP ports
	done


	unset listener_port stateful_module stateful_option
}


function welcome_text () {
	echo "[i] Hello and welcome to iptables-interactive where custom rules can be created for any system."
	echo "[i] Disclaimer: Though this script does support outbound connection filtering, many services use dynamic ports making outbound filtering difficult."
}


function create_rule () { # creates new rule
	echo "[i] Adding a rule..."
	PS3="[i] Please select a rule direction: "
	select direction in Inbound Outbound; do
		case "$direction" in
			Inbound)
				new_rule="-A INPUT"
				break
			;;
			Outbound)
				new_rule="-A OUTPUT"
				break
			;;
		esac
	done
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"

	PS3="[i] Please select a rule IP protocol: "
	select ip_protocol in TCP TCP6 UDP UDP6 ICMP; do
		case "$ip_protocol" in
			TCP)
				new_rule="iptables $new_rule -p tcp"
				break
			;;
			TCP6)
				new_rule="ip6tables $new_rule -p tcp"
				break
			;;
			UDP)
				new_rule="iptables $new_rule -p udp"
				break
			;;
			UDP6)
				new_rule="ip6tables $new_rule -p udp"
				break
			;;
			ICMP)
				new_rule="iptables $new_rule -p icmp"
				break
			;;
		esac
	done
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
	unset ip_protocol
	response=""

	until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
		read -r -p "[i] Would you like to enter a source IP address (not recommended on outbound rules)?[y/N]: " response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
		if [[ -z "$response" ]]; then
			response="n"
		fi
	done
	if [[ "$response" == 'y' ]]; then
		until [[ -n "$src_ip" ]]; do
			read -r -p "[i] Please enter a rule source IP address (ex. 192.168.1.2, ffe0::0): " src_ip
		done
		new_rule+=" -s $src_ip"
		unset src_ip
	fi
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
	response=""

	until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
		read -r -p "[i] Would you like to enter a source port (not recommended on outbound rules)?[y/N]: " response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
		if [[ -z "$response" ]]; then
			response="n"
		fi
	done
	if [[ "$response" == 'y' ]]; then
		until [[ "$src_port" -gt 0 ]] && [[ "$src_port" -lt 65536 ]]; do # until src_port is between 1-65535, prompt user
			read -r -p "[i] Please enter a rule source port (ex. 22): " src_port
		done
		new_rule+=" --sport $src_port"
		unset src_port
	fi
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
	response=""

	until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
		read -r -p "[i] Would you like to enter a destination IP address?[y/N]: " response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
		if [[ -z "$response" ]]; then
			response="n"
		fi
	done
	if [[ "$response" == 'y' ]]; then
		until [[ -n "$dst_ip" ]]; do
			read -r -p "[i] Please enter a rule destination IP address (ex. 192.168.1.2, ffe0::0): " dst_ip
		done
		new_rule+=" -d $dst_ip"
		unset dst_ip
	fi
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
	response=""

	until [[ "$dst_port" -gt 0 ]] && [[ "$dst_port" -lt 65536 ]]; do # until dst_port is between 1-65535, prompt user
		read -r -p "[i] Please enter a rule destination port (ex. 22): " dst_port
	done
	new_rule+=" --dport $dst_port"
	unset dst_port
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"

	until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
		read -r -p "[i] Would you like to specify a network interface?[y/N]: " response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
		if [[ -z "$response" ]]; then
			response="n"
		fi
	done
	if [[ "$response" == 'y' ]]; then
		until [[ -n "$net_interface" ]]; do
			read -r -p "[i] Please enter a network interface (ex. lo, eth0): " net_interface
		done
		if [[ "$direction" == "Inbound" ]]; then
			new_rule+=" -i $net_interface"
		else
			new_rule+=" -o $net_interface"
		fi
		unset net_interface direction
	fi
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
	response=""

	until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
		read -r -p "[i] Would you like to make the rule stateful?[Y/n]: " response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
		if [[ -z "$response" ]]; then
			response="y"
		fi
	done
	if [[ "$response" == 'y' ]]; then
		response=""
		until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
			echo "[i] The new rule will allow the following states: New, Established, Related"
			read -r -p "[i] Would you like to customize the allowed states?[y/N]: " response
			response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
			if [[ -z "$response" ]]; then
				response="n"
			fi
		done
		if [[ "$response" == 'y' ]]; then
			states_exit=0
			PS3="[i] Please select the states to remove: "
			until [[ "$states_exit" -eq 1 ]]; do
				select state in "${states[@]}" Done; do
					case "$state" in
						Done)
							all_states="${states[*]}"
							all_states="${all_states// /$','}"
							all_states="$(echo "$all_states" | tr '[:lower:]' '[:upper:]')" # convert to uppercase as required by iptables
							new_rule+=" -m $stateful_module $stateful_option $all_states"
							states_exit=1
							break
						;;
						*)
							cycle_states "$debug" "$state"
							break
						;;
					esac
				done
			done
			[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
		else
			new_rule+=" -m $stateful_module $stateful_option NEW,ESTABLISHED,RELATED"
		fi
	fi

	PS3="[i] Please select a rule action: "
	select action in Accept Drop Reject; do
		action="$(echo $action | tr '[:lower:]' '[:upper:]')" # convert to uppercase as required by iptables
		new_rule+=" -j $action"
		break
	done
	unset action
	[[ "${debug}" ]] && echo "[Debug] New rule: $new_rule"
	response=""

	until [[ "$response" == 'y' ]] || [[ "$response" == 'n' ]]; do # until yes or no is entered
		echo "[i] $new_rule"
		read -r -p "[i] Does the above rule look correct?[Y/n]: " response
		response="$(echo "$response" | tr '[:upper:]' '[:lower:]')" # convert to lowercase
		if [[ -z "$response" ]]; then
			response="y"
		fi
	done
	if [[ "$response" == 'n' ]]; then
		echo "[i] I'm sorry. Please start over. If you still cannot get the rule you'd like, please create a GitLab issue."
	else
		$new_rule # run the rule
	fi
	unset new_rule
}


while getopts ":dh46LMHXi" opt;	do
	case ${opt} in
		h)
			echo "Usage ./iptables-interactive.sh [-hd64L]"
			echo "  -d = debug"
			echo "  -h = help"
			echo "  -4 = no IPv4 backup"
			echo "  -6 = no IPv6 backup"
			echo "  -L = Low" # not yet implemented
			echo "  -M = Medium"
			echo "  -H = Hard"
			echo "  -X = Extreme" # does not support IP address restricting
			echo "  -i = Block all inbound ICMP"
			exit 0
		;;
		d)
			debug=true
		;;
		4)
			ipv4_backup=true
		;;
		6)
			ipv6_backup=true
		;;
		L)
			mode=low
		;;
		M)
			mode=medium
		;;
		H)
			mode=hard
		;;
		X)
			mode=extreme
		;;
		i)
			block_inbound_icmp=true
		;;
		\?)
			echo "Invalid Option: = $OPTARG 1>&2"
			exit 1
		;;
	esac
done

welcome_text
detect_firewall_packages "$debug"
backup_tables "$debug" "$ipv4_backup" "$ipv6_backup"


if grep -q -m 1 'conntrack' /proc/net/ip_tables_matches; then # checks to see if conntrack module is enabled, if not, use deprecated state module
	stateful_module="conntrack"
	stateful_option="--ctstate"
	[[ "${debug}" ]] && echo "[Debug] Using the 'conntrack' stateful module"
else
	stateful_module="state"
	stateful_option="--state"
	[[ "${debug}" ]] && echo "[Debug] Using the 'state' stateful module"
fi


reset_tables "$debug" "$stateful_module" "$stateful_option" "$mode" "$block_inbound_icmp"
declare tcp4_listeners udp4_listeners tcp6_listeners udp6_listeners proto
get_listeners "$debug"
apply_listeners "$stateful_module" "$stateful_option"


exit_script="false"
PS3="[i] Please select a service listener to deny or select an action: "
until [ "$exit_script" == "true" ]; do # run forever until Exit option is chosen
	# once the listener rules have been applied, drop everything else; done to not drop traffic states
	iptables -P INPUT DROP
	iptables -P OUTPUT DROP
	ip6tables -P INPUT DROP
	ip6tables -P OUTPUT DROP


	select menu_item in "${tcp4_listeners[@]}" "${udp4_listeners[@]}" "${tcp6_listeners[@]}" "${udp6_listeners[@]}" Create-Rule Edit-Rule Remove-Rule List-Rules Show-Counters Exit; do # display all listeners, List, and Exit; main loop
		case "$menu_item" in
			Create-Rule) # Add a custom rule
				declare -a states=(New Established Invalid Related)
				create_rule "$debug" # delete the targeted listener in global tcp6_listener variable
				break
			;;
			List-Rules) # List IPv4/6 iptables rules
				echo "[i] IPv4 current tables:"
				iptables -S
				echo ""
				echo "[i] IPv6 current tables:"
				ip6tables -S
				break
			;;
			Show-Counters) # Show IPv4/6 iptables packet counters
				echo "[i] Note, packet counters are reset on script start. Run 'ip[6]tables -vL' to see IPv4/6 packet counts."
				echo "[i] IPv4 packet counters:"
				iptables -vL
				echo ""
				echo "[i] IPv6 packet counters:"
				ip6tables -vL
				break
			;;
			Exit) # exits the script
				if ! dpkg -s iptables-persistent >/dev/null 2>&1; then
					echo "[i] Installing iptables-persistent for persistent rules through power states. Wait one moment please..."
					apt-get update | DEBIAN_FRONTEND=noninteractive apt-get install iptables-persistent -y > /dev/null
					echo "[i] Downloaded package 'iptables-persistent' to allow persistent iptables"
					iptables-save > /etc/iptables/rules.v4
					ip6tables-save > /etc/iptables/rules.v6
					echo "[i] Saved the rules. Goodbye!"
				else
					echo "[i] Goodbye!"
				fi


				exit 1
			;;
			*) # if a listener is chosen
				if echo "$menu_item" | grep -qo 'tcp6\:'; then # if listener is TCP6
					proto="tcp6"
					cycle_listeners "$debug" "$menu_item" # delete the targeted listener in global tcp6_listener variable
				elif echo "$menu_item" | grep -qo 'udp6\:'; then # if listener is UDP6
					proto="udp6"
					cycle_listeners "$debug" "$menu_item" # delete the targeted listener in global udp6_listener variable
				elif echo "$menu_item" | grep -qo 'tcp\:'; then # if listener is TCP
					proto="tcp"
					cycle_listeners "$debug" "$menu_item" # delete the targeted listener in global tcp4_listener variable
				elif echo "$menu_item" | grep -qo 'udp\:'; then # if listener is UDP
					proto="udp"
					cycle_listeners "$debug" "$menu_item" # delete the targeted listener in global udp4_listener variable
				fi


				reset_tables "$debug" "$stateful_module" "$stateful_option" "$mode" "$block_inbound_icmp"
				apply_listeners "$stateful_module" "$stateful_option"
				break
			;;
		esac
	done
done

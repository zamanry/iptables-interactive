#!/usr/bin/env python


import subprocess, sys


def execute_iptables(iptables_command):
    # need to create a way to work with stateful_module
    iptables_command = "iptables " + iptables_command
    iptables_process = subprocess(.Popen(iptables_command.split(), stdout=subprocess.PIPE)
    output, error = iptables_process.communicate()

def execute_ip6tables(ip6tables_command):
    # need to create a way to work with stateful_module
    ip6tables_command = "ip6tables " + ip6tables_command
    ip6tables_process = subprocess(.Popen(ip6tables_command.split(), stdout=subprocess.PIPE)
    output, error = ip6tables_process.communicate()


def reset_tables(debug, stateful_module, mode, block_inbound_icmp):
    execute_iptables("-P INPUT ACCEPT") # Temporarily placed to not break any traffic states
    execute_iptables("-P OUTPUT ACCEPT") # Temporarily placed to not break any traffic states
    execute_iptables("-F") # Flush all rules
    execute_iptables("-X") # Delete all chains
    execute_iptables("-Z") # Zero all counters
    execute_iptables("-P FORWARD DROP") # Drop all IP forwarding/routing since this is an end devic
    execute_iptables("-t nat -F") # Flush all NAT rules
    execute_iptables("-t nat -X") # Delete all NAT chains
    execute_iptables("-t nat -Z") # Zero all NAT counters
    execute_iptables("-t mangle -F") # Flush all mangle rules
    execute_iptables("-t mangle -X") # Delete all mangle chains
    execute_iptables("-t mangle -Z") # Zero all mangle counters

    execute_ip6tables("-P INPUT ACCEPT") # Temporarily placed to not break any traffic states
    execute_ip6tables("-P OUTPUT ACCEPT") # Temporarily placed to not break any traffic states
    execute_ip6tables("-F") # Flush all rules
    execute_ip6tables("-X") # Delete all chains
    execute_ip6tables("-Z") # Zero all counters
    execute_ip6tables("-P FORWARD DROP") # Drop all IP forwarding/routing since this is an end devic
    execute_ip6tables("-t nat -F") # Flush all NAT rules
    execute_ip6tables("-t nat -X") # Delete all NAT chains
    execute_ip6tables("-t nat -Z") # Zero all NAT counters
    execute_ip6tables("-t mangle -F") # Flush all mangle rules
    execute_ip6tables("-t mangle -X") # Delete all mangle chains
    execute_ip6tables("-t mangle -Z") # Zero all mangle counters

    if mode = "hard" or mode = "extreme": # if Hard or Extreme, implement common network attack rule
        execute_iptables("-A INPUT -p tcp ! --syn -m stateful_module NEW -j DROP") # Drop any TCP packet which don't begin with a syn flag
        execute_iptables("-A INPUT -p tcp --tcp-flags ALL ALL -j DROP") # Drop any TCP packet which has every flag set
        execute_iptables("-A INPUT -p tcp --tcp-flags ALL NONE -j DROP") # Drop any TCP glag packet which has all flags set, but a null header
        execute_iptables("-A INPUT -f -j DROP") # Drop any fragmented packet

        execute_ip6tables("-A INPUT -p tcp ! --syn -m stateful_module NEW -j DROP") # Drop any TCP packet which don't begin with a syn flag
        execute_ip6tables("-A INPUT -p tcp --tcp-flags ALL ALL -j DROP") # Drop any TCP packet which has every flag s
        execute_ip6tables("-A INPUT -p tcp --tcp-flags ALL NONE -j DROP") # Drop any TCP glag packet which has no flag set

    if block_inbound_icmp:
        # Read about ICMP states here https://www.frozentux.net/iptables-tutorial/chunkyhtml/x1582.html
        execute_iptables("-A INPUT -p icmp --icmp-type echo-request -m stateful_module NEW -j ACCEPT") # Accept echo-requests and Windows traceroute
        execute_iptables("-A INPUT -p udp --dport 33434:33523 -j REJECT") # Reject UNIX traceroute (by rejecting, tracert succeeds, but doesn't open the port)

	# IPv4 stateful rules and restrict localhost
	execute_iptables("-A INPUT -m stateful_module RELATED,ESTABLISHED -j ACCEPT")
	execute_iptables("-A OUTPUT -m stateful_module RELATED,ESTABLISHED -j ACCEPT")
	execute_iptables("-A OUTPUT -s 127.0.0.0/8 -d 127.0.0.0/8 -o lo -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT")

	# temporaryily added to not break outbound traffic
	execute_iptables("-A OUTPUT -p tcp --dport 443 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # HTTPS
	execute_iptables("-A OUTPUT -p tcp --dport 80 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # HTTP
	execute_iptables("-A OUTPUT -p udp --dport 53 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # DNS
	execute_iptables("-A OUTPUT -p udp --dport 123 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # NTP
	execute_iptables("-A OUTPUT -p udp --sport 68 --dport 67 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # DHCP
	execute_iptables("-A OUTPUT -p tcp --dport 22 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # DHCP
	execute_iptables("-A OUTPUT -p tcp --dport 1194 -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT") # DHCP
	execute_iptables("-A OUTPUT -p icmp --icmp-type echo-request -m stateful_module NEW,ESTABLISHED -j ACCEPT") #ICMP echo-re

	# IPv6 stateful rules and restrict localhost
	execute_ip6tables("-A INPUT -m stateful_module RELATED,ESTABLISHED -j ACCEPT")
	execute_ip6tables("-A OUTPUT -m stateful_module RELATED,ESTABLISHED -j ACCEPT")
	execute_ip6tables("-A OUTPUT -s ::1/128 -d ::1/128 -o lo -m stateful_module NEW,RELATED,ESTABLISHED -j ACCEPT")


	if debug: # if debug mode requested, enable logging
		execute_iptables("-A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix \'IPv4 INPUT drops: \'")
		execute_iptables("-A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix \'IPv4 OUTPUT drops: \'")
		execute_ip6tables("-A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix \'IPv6 INPUT drops: \'")
		execute_ip6tables("-A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix \'IPv6 OUTPUT drops: \'")


def main():
    exit_script = True
    while exit_script:
         execute_iptables("-P INPUT DROP")
         execute_iptables("-P OUTPUT DROP")
         execute_ip6tables("-P INPUT DROP")
         execute_ip6tables("-P OUTPUT DROP")


main()

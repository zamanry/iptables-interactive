@echo off
SetLocal
REM Method of finding last character-of-a-string-from-a-variable
REM http://stackoverflow.com/a/15662607/1569434

REM Get the script's path so all needed files can sit in the same folder
SET SCRIPTPATH=%~p0
CD %SCRIPTPATH%

REM Read and pass each line in file one at a time to sub 'FindEXE'
SET CONCATLINE=
SET HEADERROW=
SET /A LINECOUNT = 0
@echo LINECOUNT = %LINECOUNT%
del %SCRIPTPATH%\netstat*.txt /q 2>nul

netstat -bano>%SCRIPTPATH%\netstat00.txt
REM Copy all lines except those with "TIME_WAIT" into text file
FINDSTR /V /I /C:"TIME_WAIT" %SCRIPTPATH%\netstat00.txt>%SCRIPTPATH%\netstat01.txt
REM Delete first two lines, which are a header and a blank line
for /f "skip=2 delims=*" %%a in (%SCRIPTPATH%\netstat01.txt) do (echo %%a>>%SCRIPTPATH%\netstat02.txt)

REM Search for and process file based on matching text
REM This sub begins putting each netstat connection on one line
for /f "delims=*" %%A in (%SCRIPTPATH%\netstat02.txt) do call :FindTXT1 %%A

REM netstat03 will have all data from given connection on one line
SET /A LINECOUNT = 0
for /f "delims=*" %%A in (%SCRIPTPATH%\netstat03.txt) do call :FindTXT2 %%A

REM Keep only header and unique (i.e., those with "[::]") 'listening' connections
FINDSTR /I /C:"LISTENING" /C:"Local Address" %SCRIPTPATH%\netstat04.txt>%SCRIPTPATH%\netstat05.txt

REM Removes top column description line
more +1 "%SCRIPTPATH%\netstat05.txt" > "%SCRIPTPATH%\netstat-parsed.txt"

REM Cleans up
del %SCRIPTPATH%\netstat0*.txt /q 2>nul

echo done.

EndLocal
goto :EOF


:FindTXT1
REM We've got a line sent to us. Set variable to entire line using * (instead of %1)
SET CURRENTLINE=%*
SET /A LINECOUNT = %LINECOUNT% + 1
REM Add line feed after header row and return to main script
IF "%LINECOUNT%" == "1" (
    SET HEADERROW=%CURRENTLINE%
    @ECHO %CURRENTLINE%> %SCRIPTPATH%\netstat03.txt
    goto :eof
    )

REM Append a comma and CURRENTLINE to CONCATLINE. NOTE: Script expecting comma; don't use semi-colon
SET CONCATLINE=%CONCATLINE%,%CURRENTLINE%

REM When echo line, remove first char (comma, inserted above) using:
REM http://ss64.com/nt/syntax-substring.html
REM If last char is "]" then print, otherwise append
IF "%CURRENTLINE:~-1%"=="]" (
    REM @echo right bracket=FOUND
    @echo %CONCATLINE:~1%>>%SCRIPTPATH%\netstat03.txt
    SET CONCATLINE=
        ) else (
    REM @echo right bracket=NOT found
    )

REM If line = "Can not obtain ownership information" then print, otherwise append
IF "%CURRENTLINE%"=="Can not obtain ownership information" (
    REM @echo No Ownership=TRUE
    @echo %CONCATLINE:~1%>>%SCRIPTPATH%\netstat03.txt
    SET CONCATLINE=
    )

goto :eof


:FindTXT2
REM We've got a line sent to us. Set variable to entire line using * (instead of %1)
SET CURRENTLINE=%*
SET /A LINECOUNT = %LINECOUNT% + 1
REM Add line feed after header row and return to main script
IF "%LINECOUNT%" == "1" (
    SET HEADERROW=%CURRENTLINE%
    @ECHO %CURRENTLINE%> %SCRIPTPATH%\netstat04.txt
    goto :eof
    )

REM If last char is "]" then search, otherwise append.
REM Without "DelayedExp...", variable sets to value from previous FOR loop
IF "%CURRENTLINE:~-1%"=="]" (
    SetLocal ENABLEDELAYEDEXPANSION
    REM IP6 EXEs result in 3 sets of [], so find and set var to last one, which is where EXE lives
    FOR /f "tokens=1,2,3,4,5,6 delims=[]" %%a in ("%CURRENTLINE%") do (
        SET BINNAME1=%%b
        SET BINNAME2=%%f
        IF "!BINNAME1!" == "::" (
            REM @ECHO BINNAME1=!BINNAME1!>>%SCRIPTPATH%\netstat04.txt
            SET BINNAME=!BINNAME2!
            REM @echo %CURRENTLINE%;BINNAME=!BINNAME2!>>%SCRIPTPATH%\netstat04.txt
            ) else (
            SET BINNAME=!BINNAME1!
            REM @echo %CURRENTLINE%;BINNAME=!BINNAME1!>>%SCRIPTPATH%\netstat04.txt
            )
        @echo %CURRENTLINE%;BINNAME=!BINNAME!>>%SCRIPTPATH%\netstat04.txt
        )
    ) else (
    @echo %CURRENTLINE%>>%SCRIPTPATH%\netstat04.txt
    SetLocal DISABLEDELAYEDEXPANSION
)

goto :eof

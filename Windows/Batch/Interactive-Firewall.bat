@echo off

SET ARGUMENTS=%1
set nl=^&echo. :: Declares a newline character cause Batch

:: Get the script's path so all needed files can sit in the same folder
SET SCRIPT_PATH=%~p0
CD %SCRIPT_PATH%

:LOCATE_SUBSTRING :: Acting regex function
	set FULL_STRING="%~1"
	set SUBSTRING="%~2"
	set BEFORE_PATERN="%~3"
	set AFTER_PATERN="%~4"
	set "SUBSTRING=%FULL_STRING:=%BEFORE_PATERN%" & set "SUBSTRING=%"
	set "SUBSTRING=%FULL_STRING:%AFTER_PATERN%=" & set "SUBSTRING=%"
	:: Need to figure out how to return
EXIT /B 0


:PARSE_ARGUMENTS :: Parse arguments
	if not x%ARGUMENTS:h=%==x%ARGUMENTS% ( :: if `h` is in use, print help and exit.
		echo "Usage ./iptables-interactive.sh [-hd64L]" &
		echo "  -d = debug" &
		echo "  -h = help" &
		echo "  -4 = no IPv4 backup" &
		echo "  -6 = no IPv6 backup" &
		echo "  -L = Low" & :: not yet implemented
		echo "  -M = Medium" &
		echo "  -H = Hard" &
		echo "  -X = Extreme" & :: does not support IP address restricting
		echo "  -i = Block all inbound ICMP" &
		exit
		)
		if not x%ARGUMENTS:d=%==x%ARGUMENTS% ( :: if `d` is in use, proceed with debug output enabled.
			SET "DEBUG=TRUE"
			)
EXIT /B 0


:CHECK_ADMIN_RIGHTS :: Exits unless user has administrator rights
  net session >nul 2>&1 :: net session is one of the few known true options regardless of Windows OS version to confirm administrator rights.
  if %errorLevel% == 0 (
    echo "You must possess administrator rights to run this script. Exiting..." & exit)
	else (
		if defined DEBUG (echo "[Debug] User has administrator rights.")
		)
EXIT /B 0


:WELCOME_TEXT :: Displays welcome to text when script is started
	echo "Hello and welcome to iptables-interactive where custom rules can be created for any system."
	echo "Disclaimer: Though this script does support outbound connection filtering, many services use dynamic ports making filtering difficult."
EXIT /B 0


:CHECK_WINDOWS_FIREWALL :: If Windows Firewall is not used, exits
	:: Batch does not have a simple and secure method of converting from uppercase to lowercase
	:LOWERCASE_CHECK_WINDOWS_FIREWALL_USAGE
	set /p WINDOWS_FIREWALL_USAGE="Is Windows Firewall the only enabled host-based firewall?[y/n]: "

	If Not "%WINDOWS_FIREWALL_USAGE%" == "n" (
		If Not "%WINDOWS_FIREWALL_USAGE%" == "y" (echo "Please use lowercase only when replying.")
		)
	goto LOWERCASE_CHECK_WINDOWS_FIREWALL_USAGE

	If "%WINDOWS_FIREWALL_USAGE%" == "n" (echo "Since the Windows Firewall is not in use, this script cannot help you. Goodbye." & exit)
EXIT /B 0


:BACKUP_FIREWALL :: Backup the firewall rules to the user's desktop
	For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set mytime=%%a%%b)
	netsh advfirewall export "%USERPATH%\Desktop\firewall-rules-%TIMESTAMP%.wfw"
	if defined DEBUG (echo "[Debug] Firewall exported to %USERPATH%\Desktop\firewall-rules-%TIMESTAMP%.wfw")
	)
EXIT /B 0


:RESET_FIREWALL :: Reset the firewall to all basics in case of errors or malicious previous actions
	netsh advfirewall reset
	netsh advfirewall firewall delete rule name="all"
	netsh advfirewall set allprofiles logging maxfilesize "32767"
	netsh advfirewall set allprofiles logging allowedconnections enable
	::netsh advfirewall set allprofiles logging droppedconnections enable
	::netsh advfirewall set allprofiles firewallpolicy 'blockinbound,blockoutbound'
	netsh advfirewall set allprofiles settings inboundusernotification enable
	netsh advfirewall set allprofiles settings unicastresponsetomulticast disable
	netsh advfirewall set allprofiles logging filename "%SystemRoot%\System32\LogFiles\Firewall\pfirewall.log"
	netsh advfirewall set allprofiles state on
	if defined DEBUG (echo "[Debug] Firewall reset, enabled, and configured with basic logging at %SystemRoot%\System32\LogFiles\Firewall\pfirewall.log")
EXIT /B 0


:GET_PARSE_LISTENERS :: Get TCP/TCP6/UDP/UDP6 listeners and parse netstat into a useable format
	call %SCRIPT_PATH%\Windows\Batch\netstat-parser.bat :: Runs `netstat -bano` and parses it nicely
	if defined DEBUG (echo "[Debug] Finished executing and basic parsing of netstat at %SCRIPT_PATH%\Windows\Batch\netstat-parsed.txt")
	copy NUL %SCRIPT_PATH%\Windows\Batch\TCP_Listeners.txt
	copy NUL %SCRIPT_PATH%\Windows\Batch\TCP6_Listeners.txt
	copy NUL %SCRIPT_PATH%\Windows\Batch\UDP_Listeners.txt
	copy NUL %SCRIPT_PATH%\Windows\Batch\UDP6_Listeners.txt
	for /F "tokens=*" %%A in (%SCRIPT_PATH%\Windows\Batch\netstat-parsed.txt) do (
		set "LINE=%%A"
	  set "PROTOCOL=%%LINE:~0,4%%"
	  if "%PROTOCOL%" == "TCP" ( :: If TCP (v4 or v6)...
			set "FIRST_CHAR_OF_LOCAL_ADDRESS=%%LINE:~7,8%%"
			if not "%FIRST_CHAR_OF_LOCAL_ADDRESS%" == "[" ( :: If not IPv6...
				echo %LINE% %nl%>>TCP_Listeners.txt :: Echo netstat line into the TCP file
				if defined DEBUG (echo "[Debug] This listener's protocol is %PROTOCOL% and the first local address character is %FIRST_CHAR_OF_LOCAL_ADDRESS%. Therefore, added to TCP_Listeners.")
			)
			else ( :: If the TCP listener is IPv6...
				echo %LINE% %nl%>>TCP6_Listeners.txt :: Echo netstat line into the TCP6 file
				if defined DEBUG (echo "[Debug] This listener's protocol is %PROTOCOL% and the first local address character is %FIRST_CHAR_OF_LOCAL_ADDRESS%. Therefore, added to TCP6_Listeners.")
			)
		)
		else (
			if not "%FIRST_CHAR_OF_LOCAL_ADDRESS%" == "[" ( :: If not IPv6...
				echo %LINE% %nl%>>UDP_Listeners.txt :: Echo netstat line into the UDP file
				if defined DEBUG (echo "[Debug] This listener's protocol is %PROTOCOL% and the first local address character is %FIRST_CHAR_OF_LOCAL_ADDRESS%. Therefore, added to UDP_Listeners.")
			)
			else ( :: If the TCP listener is IPv6...
				echo %LINE% %nl%>>UDP6_Listeners.txt :: Echo netstat line into the UDP6 file
				if defined DEBUG (echo "[Debug] This listener's protocol is %PROTOCOL% and the first local address character is %FIRST_CHAR_OF_LOCAL_ADDRESS%. Therefore, added to UDP6_Listeners.")
			)
		)
	) %%A
EXIT /B 0


:: The main `function` is below
call :CHECK_ADMIN_RIGHTS
call :PARSE_ARGUMENTS
call :WELCOME_TEXT
call :CHECK_WINDOWS_FIREWALL

:: Get current timestamp
for /F "tokens=2" %%i in ('date /t') do set mydate=%%i
set TIMESTAMP=%mydate%_%time%
set TIMESTAMP=%TIMESTAMP:/=- % :: Replace slash with dash for filename usage
set TIMESTAMP=%TIMESTAMP::=- % :: Replace colon with dash for filename usage
if defined DEBUG (echo "[Debug] The date and time is: %TIMESTAMP%")

call :BACKUP_FIREWALL
call :RESET_FIREWALL
call GET_PARSE_LISTENERS

# iptables-interactive

An interactive Bash script to block any unwanted TCP and UDP listeners on an endpoint. Different firewall intensity levels can be requested for different levels of scrutiny.

# Current Features
*  Does not drop any existing listener traffic until manually selected for removal
*  Medium and hard intensity levels
*  TCPv4/6, UDPv4/6, and ICMPv4/6
*  Firewall logging
*  Listing rules and displaying rule packet counters
*  Debug and help modes

# Firewall Intensity Levels
*  Low - Not stateful, supports all ICMP codes/types, and any desired IPv4/IPv6 ports
*  Medium - Stateful, supports all ICMP codes/types, and any desired IPv4/IPv6 ports
*  Hard - Stateful, supports select ICMP codes/types, common network attack prevention rules, and any desired IPv4/IPv6 ports
*  Extreme - Stateful, supports select ICMP codes/types, common network attack prevention rules, and any IP address restricted IPv4/IPv6 ports

# Requirements
*  Bash
*  netstat
*  iptables
*  iptables-persistent

# Upcoming Features (Ordered)
*  Python 2.7 and 3 support in one script using virtual environments
*  ICMPv4/6 type/code filtering
*  Complete low, medium, hard, and extreme firewall level intensities
*  Support of ss and lsof
*  Restriction of outbound network connections
*  Multiple interface support

# Long-Term Goals
*  Support for all UNIX and Linux flavors
*  Minimal code reuse
*  Efficient code
*  Well commented code
*  Support for non-TCP/UDP protocols like AH and ESP
*  Choice between firewalld, iptables, nftables, and UFW
*  Windows support (>= Windows XP)
*  A cool repository name independent from a single firewall service or operating system

# Does not Support
*  NAT, routing, etc.
